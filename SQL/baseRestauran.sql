




create table Serveur(
	idServeur INT AUTO_INCREMENT PRIMARY KEY,
	nom varchar(50),
    email varchar(50),
    mdp varchar(50)
);


create table Profil(
    idProfil INT AUTO_INCREMENT PRIMARY KEY,
    idServeur int,
    administrator int,
    creer int,
    lire int,
    modifier int,
    supprimer int,
    foreign key(idServeur) references Serveur(idServeur)
);


    create table Categorie(
        idCategorie INT AUTO_INCREMENT PRIMARY KEY,
        nom varchar(50)
    );


create table Tables(
    idTable INT AUTO_INCREMENT PRIMARY KEY,
    nom varchar(50)
);



create table Programme(
    idProgramme INT AUTO_INCREMENT PRIMARY KEY,
    Jour Date
);

create table Plat(
    idPlat INT AUTO_INCREMENT PRIMARY KEY,
    idCategorie int,
    nom varchar(50),
    prix decimal,
    foreign key(idCategorie) references Categorie(idCategorie)
);


create table Commande(
    idCommande INT AUTO_INCREMENT PRIMARY KEY,
    idPlat int,
    idServeur int,
    idTable int,
    idProgramme int,
    nombre int,
    etat int,
    foreign key(idPlat) references Plat(idPlat),
    foreign key(idServeur) references Serveur(idServeur),
    foreign key(idTable) references Tables(idTable),
    foreign key(idProgramme) references Programme(idProgramme)
);

create table Facture(
    idFacture INT AUTO_INCREMENT PRIMARY KEY,
    idCommande int,
    idTable int,
    Montant decimal,
    foreign key(idCommande) references Commande(idCommande),
    foreign key(idTable) references Tables(idTable)
);



insert into Serveur values(null,'Marie','marie@gmail.com',SHA1('1234'));
insert into Serveur values(null,'Michou','michou@gmail.com',SHA1('1234'));
insert into Serveur values(null,'Edwige','edwige@gmail.com',SHA1('1234'));
insert into Serveur values(null,'Florette','florette@gmail.com',SHA1('1234'));
insert into Serveur values(null,'Nanah','nanah@gmail.com',SHA1('1234'));


insert into Profil values(null,1,1,0,0,0,0);
insert into Profil values(null,2,0,0,0,0,0);
insert into Profil values(null,3,0,0,0,0,0);
insert into Profil values(null,4,0,0,0,0,0);
insert into Profil values(null,5,0,0,0,0,0);

insert into Categorie values(null,'Dinner');
insert into Categorie values(null,'Drinks');
insert into Categorie values(null,'Snackes');
insert into Categorie values(null,'Dessert');

insert into Tables values(null,"TableA");
insert into Tables values(null,"TableB");
insert into Tables values(null,"TableC");
insert into Tables values(null,"TableD");
insert into Tables values(null,"TableE");
insert into Tables values(null,"TableF");


insert into Plat values(null,1,'Salmon Tomatoes',10000);
insert into Plat values(null,1,'Greek_Chiken&Potatoes',15000);
insert into Plat values(null,1,'Baked Macaroni and Cheese',10000);
insert into Plat values(null,1,'Vegetarian Korma',10000);
insert into Plat values(null,1,'Fish Tacos',15000);
insert into Plat values(null,1,'Simple Humburger',15000);
insert into Plat values(null,1,'Chiken Parmesan',20000);
insert into Plat values(null,1,'Brown Sugar Meatloaf',15000);
insert into Plat values(null,1,'Broiled Tilapia',20000);
insert into Plat values(null,1,'Pasta Main Dishes',10000);


insert into Plat values(null,2,'Soda',8000);
insert into Plat values(null,2,'Beer',8000);
insert into Plat values(null,2,'Coffee',3000);
insert into Plat values(null,2,'Chocolate',3000);
insert into Plat values(null,2,'Milk',3000);
insert into Plat values(null,2,'Tea',2000);
insert into Plat values(null,2,'Juice',2000);
insert into Plat values(null,2,'Wine',10000);
insert into Plat values(null,2,'Milkshakes',3000);
insert into Plat values(null,2,'Cocoa',3000);

insert into Plat values(null,3,'Chips',3000);
insert into Plat values(null,3,'Cacapigeon',2000);
insert into Plat values(null,3,'Pistaches',2000);
insert into Plat values(null,3,'Cookies',4000);
insert into Plat values(null,3,'Lays',3000);
insert into Plat values(null,3,'Popcorn',3000);
insert into Plat values(null,3,'Crackers',2000);
insert into Plat values(null,3,'Crisps',2000);
insert into Plat values(null,3,'Chocolate',5000);
insert into Plat values(null,3,'Frites',3000);


insert into Plat values(null,4,'Chocolate_Mint Bars',5000);
insert into Plat values(null,4,'Ice cream cake',8000);
insert into Plat values(null,4,'Banana pudding',5000);
insert into Plat values(null,4,'Ice cream',4000);
insert into Plat values(null,4,'Orange Torta',3000);
insert into Plat values(null,4,'Apple Tart',3000);
insert into Plat values(null,4,'Fruit jelly&Ice Cream',2000);
insert into Plat values(null,4,' Chocolate Fondu&Fruit',4000);
insert into Plat values(null,4,' Strawberry Tarts',3000);
insert into Plat values(null,4,'Yaourt',2000);


create view Menu as(select Plat.idPlat,Categorie.idCategorie,Categorie.nom categorie,Plat.nom,Plat.prix 
from Categorie join Plat on Categorie.idCategorie=Plat.idCategorie);