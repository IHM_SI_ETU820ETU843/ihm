<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deconnect_controller extends CI_Controller {
	function __construct() { 
		parent::__construct(); 
		$this->load->helper('url'); 
		
		$this->load->library ('session');
    }
    public function disconnect(){
        $this->session->sess_destroy();
        $this->load->view('welcome_login');
    } 
    }
?>