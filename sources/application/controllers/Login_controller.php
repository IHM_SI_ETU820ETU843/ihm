<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() { 
		parent::__construct(); 
		$this->load->helper('url'); 
		$this->load->database(); 
		$this->load->library ('session');
	 } 
	 
	public function log() { 
		$email = $this->input->post('email');
		$mdp = $this->input->post('pass');
		$this->load->model('Login_Model');
		$this->load->model('Plat_Model');
		$this->load->model('Table_model');
		$data['User']=$this->Login_Model->validate($email,$mdp);	
		
		$plats['plat1']=$this->Plat_Model->getPlatJour("Dinner");
		$plats['plat2']=$this->Plat_Model->getPlatJour("Drinks");
		$plats['plat3']=$this->Plat_Model->getPlatJour("Snackes");
		$plats['plat4']=$this->Plat_Model->getPlatJour("Dessert");
		$plats['tables']=$this->Table_model->getTables();
		if(count($data['User'])==1){
			$this->session->set_userdata('serveur',$data['User']);
			$this->load->view("afficher",$plats);
			
		}else{
			$this->load->view('welcome_login');
		}
	 } 

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('welcome_login');
	}
}
